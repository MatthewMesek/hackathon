const { expect } = require('chai')
const request = require('supertest')

// App
const app = require('../app')
const agent = request.agent(app)

// Test suite
describe('API', () => {
  it('should say `Hello Hackathon`', async () => {
    const res = await request(app).get('/api')
    expect(res.status).to.equal(200)
    expect(res.text).to.be.a('string')
    expect(res.text).to.equal('Hello Hackathon')
  })

  it('should echo a query', done => {
    agent
      .get('/api')
      .query({ q: 'echo' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('echo')
        done()
      })
  })

  it('should say my name', done => {
    agent
      .get('/api')
      .query({ q: 'dee4e4f0: what is your name' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('MattM')
        done()
      })
  })

  it('what is X plus Y', done => {
    agent
      .get('/api')
      .query({ q: 'dee4e4f0: what is 0 plus 9' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('9')
        done()
      })
  })

  it('what is X multiplied by Y', done => {
    agent
      .get('/api')
      .query({ q: 'ab743ee0: what is 9 multiplied by 17' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('153')
        done()
      })
  })

  it('what is X minus Y', done => {
    agent
      .get('/api')
      .query({ q: 'ab743ee0: what is 15 minus 12' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('3')
        done()
      })
  })

  it('what is X to the power of Y', done => {
    agent
      .get('/api')
      .query({ q: 'e7888ff0: what is 7 to the power of 16' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('33232930569601')
        done()
      })
  })

  it('what is X plus Y plus Z', done => {
    agent
      .get('/api')
      .query({ q: 'e7888ff0: what is 17 plus 2 plus 18' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('37')
        done()
      })
  })

  it('what is X multiplied by Y multiplied by Z', done => {
    agent
      .get('/api')
      .query({ q: 'e7888ff0: what is 17 multiplied by 2 multiplied by 18' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('612')
        done()
      })
  })

  it('what is X to the power of Y to the power of Z', done => {
    agent
      .get('/api')
      .query({ q: 'e7888ff0: what is 1 to the power of 2 to the power of 3' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('1')
        done()
      })
  })

  it('what is X minus Y to the power of Z', done => {
    agent
      .get('/api')
      .query({ q: 'e7888ff0: what is 1 minus 2 to the power of 3' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('7')
        done()
      })
  })

  it('what is X minus Y minus Z', done => {
    agent
      .get('/api')
      .query({ q: 'e7888ff0: what is 17 minus 2 minus 18' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('-3')
        done()
      })
  })

  it('which of the following numbers is the largest: X, Y, Z', done => {
    agent
      .get('/api')
      .query({ q: 'dee4e4f0: which of the following numbers is the largest: 50, 448, 10, 887' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('887')
        done()
      })
  })

  it('which of the following numbers is both a square and a cube: X, Y, Z', done => {
    agent
      .get('/api')
      .query({ q: 'dee4e4f0: which of the following numbers is both a square and a cube: 867, 262144, 397, 2304' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('2304')
        done()
      })
  })

  it('which of the following numbers are primes: X, Y, Z', done => {
    agent
      .get('/api')
      .query({ q: 'dee4e4f0: which of the following numbers are primes: 191, 610, 257, 601' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('191,257,601')
        done()
      })
  })

  it('what is the X number in the Fibonacci sequence', done => {
    agent
      .get('/api')
      .query({ q: 'ef250210: what is the 14th number in the Fibonacci sequence' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('377')
        done()
      })
  })

  it('what colour is a banana', done => {
    agent
      .get('/api')
      .query({ q: '11950000: what colour is a banana' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('yellow')
        done()
      })
  })

  //

  context('should error', () => {
    it('on request to a non-existing endpoint', done => {
      agent
        .get('/api/bla')
        .expect(404)
        .end(done)
    })
  })
})

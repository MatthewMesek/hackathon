const { expect } = require('chai')
const request = require('supertest')

// App
const app = require('../app')
const agent = request.agent(app)

// Test suite
describe('Home page', () => {
  it('should respond with a success status', async () => {
    const res = await request(app).get('/')
    expect(res.status).to.equal(200)
  })

  it('should say `Hackathon`', done => {
    agent
      .get('/')
      .expect('Content-Type', /text\/html/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.contain('Hackathon')
        done()
      })
  })

  context('should error', () => {
    it('on request to a non-existing page', done => {
      agent
        .get('/bla')
        .expect(404)
        .end(done)
    })
  })
})

module.exports = {
  hackathon: (req, res) => {
    function isPrime(num) {
      for (let i = 2; i < num; i++) {
        if (num % i === 0) return false
      }

      return num > 1
    }

    function fib(n) {
      let arr = [0, 1]
      for (let i = 2; i < n + 1; i++) {
        arr.push(arr[i - 2] + arr[i - 1])
      }
      return arr[n]
    }

    function cal(x, y, z) {
      switch (x) {
        case 'plus':
          return Number(y) + Number(z)

        case 'multiplied by':
          return Number(y) * Number(z)

        case 'minus':
          return Number(y) - Number(z)

        case 'to the power of':
          return Math.pow(Number(y), Number(z))
      }
    }

    let query = req.query
    let question = req.query.q

    let answer = 'Hello Hackathon'
    let parts
    let message
    let regexpResponse

    if (question) {
      answer = question

      parts = question.split(':')

      if (parts.length > 1) {
        message = parts
          .slice(1)
          .join(':')
          .trim()

        switch (message) {
          case 'what is your name':
            answer = 'MattM'
            break

          case 'what colour is a banana':
            answer = 'yellow'
            break
        }

        if ((regexpResponse = /^what is ([0-9]+) (plus|multiplied by|minus|to the power of) ([0-9]+)$/.exec(message))) {
          switch (regexpResponse[2]) {
            case 'plus':
              answer = Number(regexpResponse[1]) + Number(regexpResponse[3])
              break

            case 'multiplied by':
              answer = Number(regexpResponse[1]) * Number(regexpResponse[3])
              break

            case 'minus':
              answer = Number(regexpResponse[1]) - Number(regexpResponse[3])
              break

            case 'to the power of':
              answer = Math.pow(Number(regexpResponse[1]), Number(regexpResponse[3]))
              break
          }
        } else if (
          (regexpResponse = /^what is ([0-9]+) (plus|multiplied by|minus|to the power of) ([0-9]+) (plus|multiplied by|minus|to the power of) ([0-9]+)$/.exec(
            message
          ))
        ) {
          answer = 0

          let c1 = regexpResponse[2]
          let v1 = regexpResponse[1]
          let v2 = regexpResponse[3]
          let c2 = regexpResponse[4]
          let v3 = regexpResponse[5]

          if (c1 !== 'multiplied by' && c1 !== 'to the power of' && (c2 === 'multiplied by' || c2 === 'to the power of')) {
            answer = cal(c2, v2, v3)
            answer = cal(c1, answer, v1)
          } else {
            answer = cal(c1, v1, v2)
            answer = cal(c2, answer, v3)
          }
        } else if ((regexpResponse = /^which of the following numbers is the largest: ([0-9, ]+)/.exec(message))) {
          let numbers = regexpResponse[1].split(',').map(function(v) {
            return Number(v.trim())
          })

          answer = Math.max(...numbers)
        } else if ((regexpResponse = /^which of the following numbers is both a square and a cube: ([0-9, ]+)/.exec(message))) {
          let numbers = regexpResponse[1].split(',').map(function(v) {
            return Number(v.trim())
          })

          for (let i = 0; i < numbers.length; i++) {
            let number = numbers[i]

            if (Math.sqrt(number) % 1 === 0 && (number * number * number * number) % 1 === 0) {
              answer = number
            }
          }
        } else if ((regexpResponse = /^which of the following numbers are primes: ([0-9, ]+)/.exec(message))) {
          let numbers = regexpResponse[1].split(',').map(function(v) {
            return Number(v.trim())
          })

          answer = []

          for (let i = 0; i < numbers.length; i++) {
            let number = numbers[i]

            if (isPrime(number)) {
              answer.push(number)
            }
          }
        } else if ((regexpResponse = /^what is the ([0-9]+)(nd|rd|th) number in the Fibonacci sequence$/.exec(message))) {
          answer = fib(regexpResponse[1])
        }
      }
    }

    // Log a query from game server
    if (process.env.NODE_ENV != 'test') console.log(query, parts, message, regexpResponse, answer) // eslint-disable-line no-console

    // Answer the question
    res.status(200).send(String(answer))
  }
}
